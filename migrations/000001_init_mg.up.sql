CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE IF NOT EXISTS patients(
    id uuid DEFAULT uuid_generate_v4(),
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    age INTEGER NOT NULL,
    gender TEXT NOT NULL,
    email TEXT NOT NULL,
    illiness_name TEXT NOT NULL,
    diagnosis TEXT NOT NULL,
    phone_number TEXT NOT NULL,
    building_id TEXT NOT NULL,
    room_id TEXT NOT NULL,
    doctor_id TEXT NOT NULL,  
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE IF NOT EXISTS patient_history(
    patient_id TEXT NOT NULL,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    age INTEGER NOT NULL,
    gender TEXT NOT NULL,
    email TEXT NOT NULL,
    illiness_name TEXT NOT NULL,
    diagnosis TEXT NOT NULL,
    phone_number TEXT NOT NULL,
    room_id TEXT NOT NULL,
    doctor_id TEXT NOT NULL,
    medication_id TEXT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP 
);
