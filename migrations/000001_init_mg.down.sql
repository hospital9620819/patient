DROP TABLE IF EXISTS patients;
DROP TABLE IF EXISTS patient_history;
DROP TABLE IF EXISTS schema_migrations;