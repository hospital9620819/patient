package service

import (
	"context"
	"fmt"
	"log"

	illines "hospital/patient/genproto/illines_service"
	pb "hospital/patient/genproto/patient"

	l "hospital/patient/pkg/logger"
	grpcclient "hospital/patient/service/grpc_client"
	"hospital/patient/storage"

	"github.com/jmoiron/sqlx"
)


type PatientService struct {
	storage storage.IStorage
	logger  l.Logger
	Client  grpcclient.Clients
}


// Patient info...
func NewPatientService(db *sqlx.DB, log l.Logger, client grpcclient.Clients) *PatientService {
	return &PatientService{
		storage: storage.NewStoragePg(db),
		logger:  log,
		Client:  client,
	}
}

func (p *PatientService) CreatePatient(ctx context.Context, req *pb.Patient) (*pb.PatientResponse, error) {

	resPatient, err := p.storage.Patient().CreatePatient(req)
	if err != nil {
		log.Println("failed to creating Patient: ", err)
		return &pb.PatientResponse{}, err
	}

	illinesName := resPatient.IllinessName
	resMedicationId, err := p.Client.Illines().GetMedicationById(ctx, &illines.IllinesName{IllinesName: illinesName})
	if err != nil {
		log.Println("failed to get medication for patient : ", err)
		return &pb.PatientResponse{}, err
	}
	//fmt.Println("<<<<<<<<<", resMedicationId.IllinesId)

	response := &pb.PatientHistoryReq{
		PatientId:    resPatient.PatientId,
		FirstName:    resPatient.FirstName,
		LastName:     resPatient.LastName,
		Age:          resPatient.Age,
		Gender:       resPatient.Gender,
		Email:        resPatient.Email,
		IllinessName: resPatient.IllinessName,
		Diagnosis:    resPatient.Diagnosis,
		PhoneNumber:  resPatient.PhoneNumber,
		RoomId:       resPatient.RoomId,
		DoctorId:     resPatient.DoctorId,
		MedicationId: resMedicationId.IllinesId,
	}

	resPatientHistory, err := p.storage.Patient().CreatePatientHistory(response)
	fmt.Println(">>>>>>", resPatientHistory.MedicationId)

	if err != nil {
		log.Println("failed to creating patient info : ", err)
	}

	return resPatient, nil
}

func (p *PatientService) GetPatientByName(ctx context.Context, req *pb.PatientName) (*pb.PatientResponse, error) {
	response, err := p.storage.Patient().GetPatientByName(req)

	if err != nil {
		log.Println("failed to getting Patient: ", err)
		return &pb.PatientResponse{}, err
	}

	return response, nil
}

func (p *PatientService) GetAllPatients(ctx context.Context, req *pb.PatientListRequest) (*pb.Patients, error) {
	response, err := p.storage.Patient().GetAllPatients(req)

	if err != nil {
		log.Println("Failed to get all patient: ", err)
		return &pb.Patients{}, err
	}

	return response, nil
}

func (p *PatientService) UpdatePatient(ctx context.Context, req *pb.UpdatePatientInfo) (*pb.PatientResponse, error) {
	response, err := p.storage.Patient().UpdatePatient(req)
	if err != nil {
		log.Println("Failed to update patient:", err)
		return &pb.PatientResponse{}, err
	}

	res, err := p.storage.Patient().UpdatePatientHistory(response)
	if err != nil {
		log.Println("fail to update patient history:", err)
	}

	return res, nil
}

func (p *PatientService) DeletePatient(ctx context.Context, req *pb.PatientName) (*pb.PatientResponse, error) {
	response, err := p.storage.Patient().DeletePatient(req)
	if err != nil {
		log.Println("Failed to delete patient: ", err)
		return &pb.PatientResponse{}, err
	}

	res, err := p.storage.Patient().DeletePatientHistory(response)

	return response, nil
}

// Patient history info...
func (p *PatientService) GetPatientHistory(ctx context.Context, req *pb.PatientName) (*pb.PatientHistoryRes, error) {
	res, err := p.storage.Patient().GetPatientHistory(req)
	if err != nil {
		log.Println("error while get patient history info:", err)
	}

	return res, nil
}

func (p *PatientService) GetAllPatientHistory(ctx context.Context, req *pb.PatientListRequest) (*pb.PatientsHistoryList, error) {
	res, err := p.storage.Patient().GetAllPatientHistory(req)
	if err != nil {
		log.Println("fail to get all patient history:", err)
	}

	return res, nil
}






// func (s *PatientService) Login(ctx context.Context, req *pb.LoginRequest) (*pb.PatientRequest, error) {
// 	req.Email = strings.ToLower(req.Email)
// 	req.Email = strings.TrimSpace(req.Email)

// 	patient, err := s.storage.Patient().GetByEmail(req)
// 	if err != nil {
// 		log.Println("Failed login by email: ", err)
// 		return &pb.PatientRequest{}, nil
// 	}
// 	// fmt.Println(req.Email)
// 	// fmt.Println(req.Password)
// 	// fmt.Println(patient.Password)

// 	err = bcrypt.CompareHashAndPassword([]byte(patient.Password), []byte(req.Password))
// 	if err != nil {
// 		log.Println("Failed password:>>> ", err)
// 		return &pb.PatientRequest{}, nil
// 	}

// 	return patient, nil
// }

// func (s *PatientService) UpdatePatientTokens(ctx context.Context, req *pb.UpdatePatientTokensReq) (*pb.UpdatePatientTokensReq, error) {
// 	res, err := s.storage.Patient().UpdatePatientTokens(req)
// 	if err != nil {
// 		s.logger.Error("error updating info", logger.Any("Error while update info", err))
// 		return &pb.UpdatePatientTokensReq{}, err
// 	}

// 	return res, nil
// }

// func (s *PatientService) GetPatientById(ctx context.Context, req *pb.PatientIdReq) (*pb.PatientRequest, error) {
// 	res, err := s.storage.Patient().GetPatientById(req)
// 	if err != nil {
// 		s.logger.Error("error get by id info", logger.Any("Error while get info by id", err))
// 		return &pb.PatientRequest{}, err
// 	}

// 	return res, nil
// }
