package grpcclient

import (
		"fmt"
		"hospital/patient/config"
		il "hospital/patient/genproto/illines_service"

		"google.golang.org/grpc"
		"google.golang.org/grpc/credentials/insecure"
)

type Clients interface {
	Illines() il.DiseaseServiceClient	
}

type ServiceManager struct {
	Config config.Config
	illinesService il.DiseaseServiceClient
}

func New(cfg config.Config) (*ServiceManager, error) {
	connIllines, err := grpc.Dial(
		fmt.Sprintf("%s:%s", cfg.IllinesServiceHost, cfg.IllinesServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("illines service dial host: %s port: %s ", cfg.IllinesServiceHost, cfg.IllinesServicePort)
	}

	return &ServiceManager{
		Config: cfg,
		illinesService: il.NewDiseaseServiceClient(connIllines),
	}, nil
}

func (s *ServiceManager) Illines() il.DiseaseServiceClient {
	return s.illinesService
}
