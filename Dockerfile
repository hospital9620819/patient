FROM golang:1.20-alpine
RUN mkdir patient
COPY . /patient
WORKDIR /patient
RUN go build -o main cmd/main.go
CMD ./main
EXPOSE 5000