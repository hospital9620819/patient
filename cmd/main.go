package main

// ........
import (
	"net"

	"hospital/patient/config"
	p "hospital/patient/genproto/patient"
	"hospital/patient/pkg/db"
	"hospital/patient/pkg/logger"
	"hospital/patient/service"
	grpcclient "hospital/patient/service/grpc_client"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()
	log := logger.New(cfg.LogLevel, "patient_service")
	defer logger.Cleanup(log)

	log.Info("main: sqlxConfig",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", int(cfg.PostgresPort)),
		logger.String("database", cfg.PostgresDatabase))

	connDB, err := db.ConnectToDB(cfg)
	if err != nil {
		log.Fatal("sqlx connection to postgres error: %v", logger.Error(err))
	}

	grpcClient, err := grpcclient.New(cfg)
	if err != nil {
		log.Fatal("Failed while grpc client:  %v", logger.Error(err))
	}

	patientService := service.NewPatientService(connDB, log, grpcClient)

	lis, err := net.Listen("tcp", cfg.PatientServicePort)
	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)
	p.RegisterPatientServiceServer(s, patientService)

	log.Info("main: server running", logger.String("port", cfg.PatientServicePort))
	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

}
