 package postgres

// import (
// 	"fmt"
// 	pb "hospital/patient/genproto/patient"
// 	"log"
// )

// func (r *PatientRepo) CreatePatientHistory(patient *pb.PatientHistoryReq) (*pb.PatientHistoryRes, error) {
// 	var response pb.PatientHistoryRes
// 	err := r.db.QueryRow(`
// 		INSERT INTO patient_history(patient_id, patient_name, illiness_name, method, result)
// 		VALUES ($1, $2, $3, $4, $5)
// 		RETURNING patient_id, patient_name, illiness_name, method, result, created_at, updated_at`,
// 		patient.PatientId, patient.PatientName, patient.IllinessName, patient.Method, patient.Result).
// 		Scan(&response.PatientId, &response.PatientName, &response.IllinessName, &response.Method,
// 			&response.Result, &response.CreatedAt, &response.UpdatedAt)

// 	if err != nil {
// 		log.Println("Failed to create patient info; ", err)
// 		return &pb.PatientHistoryRes{}, err
// 	}

// 	return &response, nil
// }

// func (r *PatientRepo) GetPatientHistory(patient *pb.PatientHistoryName) (*pb.PatientHistoryRes, error) {
// 	var response pb.PatientHistoryRes
// 	err := r.db.QueryRow(`
// 		SELECT patient_id, patient_name, illiness_name, method, result, created_at, updated_at
// 		FROM patient_history
// 		WHERE patient_name = $1 and deleted_at IS NULL`, patient.PatientName).Scan(
// 		&response.PatientId, &response.PatientName, &response.IllinessName, &response.Method, &response.Result,
// 		&response.CreatedAt, &response.UpdatedAt)

// 	if err != nil {
// 		log.Println("Failed to get patient history info")
// 		return &pb.PatientHistoryRes{}, err
// 	}

// 	return &response, nil
// }

// func (r *PatientRepo) GetAllPatientHistory(patient *pb.PatientHistoryListReq) (*pb.PatientsHistory, error) {
// 	var response pb.PatientsHistory
// 	offset := (patient.Page - 1) * patient.Limit

// 	rows, err := r.db.Query(`
// 		SELECT patient_id, patient_name, illiness_name, method, result, created_at, updated_at
// 		FROM patient_history
// 		WHERE deleted_at IS NULL
// 		LIMIT $1 OFFSET $2`, patient.Limit, offset)

// 	fmt.Println(rows)
// 	if err != nil {
// 		log.Println("Failed to get all patient info: ", err)
// 		return &pb.PatientsHistory{}, nil
// 	}

// 	for rows.Next() {
// 		res := pb.PatientHistoryRes{}
// 		err = rows.Scan(
// 			&res.PatientId, &res.PatientName, &res.IllinessName,
// 			&res.Method, &res.Result, &res.CreatedAt,
// 			&res.UpdatedAt)

// 		if err != nil {
// 			log.Println("Failed to get all patient info")
// 			return &pb.PatientsHistory{}, nil
// 		}

// 		response.PatientsHistory = append(response.PatientsHistory, &res)
// 	}

// 	return &response, nil

// }

// func (r *PatientRepo) UpdatePatientHistory(patient *pb.UpdatePatientHirtory) (*pb.PatientHistoryRes, error) {
// 	res := pb.PatientHistoryRes{}

// 	err := r.db.QueryRow(`
// 		UPDATE patient_history
// 		SET method = $1, result = $2, illiness_name = $3
// 		WHERE patient_id = $4 AND deleted_at IS NULL
// 		RETURNING patient_id,patient_name,illiness_name,method,result,updated_at`, patient.Method, patient.Result, patient.IllinessName, patient.PatientId).Scan(
// 		&res.PatientId, &res.PatientName, &res.IllinessName, &res.Method, &res.Result, &res.UpdatedAt)

// 	if err != nil {
// 		log.Println("Failed to update patient info: ", err)
// 		return &pb.PatientHistoryRes{}, err
// 	}

// 	return &res, err
// }

// func (r *PatientRepo) DeletePatientHistory(patient *pb.PatientHistoryName) (*pb.PatientHistoryRes, error) {
// 	res := pb.PatientHistoryRes{}

// 	err := r.db.QueryRow(`
// 		UPDATE patient_history
// 		SET deleted_at = current_timestamp
// 		WHERE patient_name = $1
// 		RETURNING patient_id,patient_name,illiness_name,method,result,updated_at, deleted_at`, patient.PatientName).Scan(
// 		&res.PatientId, &res.PatientName, &res.IllinessName, &res.Method, &res.Result, &res.UpdatedAt, &res.DeletedAt)

// 	if err != nil {
// 		log.Println("Failed to delete patient histoy info")
// 		return &pb.PatientHistoryRes{}, err
// 	}

// 	return &res, nil

// }

// func (r *PatientRepo) SearchPatientHistory(patient *pb.PatientHistoryName) (*pb.PatientHistoryRes, error) {
// 	var res pb.PatientHistoryRes

// 	err := r.db.QueryRow(`
// 	SELECT patient_id, patient_name, illiness_name,method,result,created_at,updated_at
// 	FROM patient_history
// 	WHERE patient_name LIKE $1 AND deleted_at IS NULL`, patient.PatientName).Scan(
// 		&res.PatientId, &res.PatientName, &res.IllinessName, &res.Method, &res.Result, &res.CreatedAt, &res.UpdatedAt)

// 	if err != nil {
// 		log.Println("Failed to searching patient info:. ", err)
// 		return &pb.PatientHistoryRes{}, err
// 	}

// 	return &res, nil
// }
