package postgres

import (
	"hospital/patient/config"
	"hospital/patient/pkg/db"
	"hospital/patient/pkg/logger"
	"log"
	"os"
	"testing"
)

var pgRepo *PatientRepo

func TestMain(m *testing.M) {
	conf := config.Load()
	connDB, err := db.ConnectToDB(conf)

	if err != nil {
		log.Fatal("Error while connect to database: ", logger.Error(err))
	}

	pgRepo = NewPatientRepo(connDB)
	os.Exit(m.Run())

}
