package postgres

import (
	"github.com/jmoiron/sqlx"
)

type PatientRepo struct {
	db *sqlx.DB
}

func NewPatientRepo(db *sqlx.DB) *PatientRepo {
	return &PatientRepo{
		db: db,
	}
}
