package postgres

// import (
// 	pb "hospital/patient/genproto/patient"
// 	"reflect"
// 	"testing"
// )

// func TestPatientHistory_Create(t *testing.T) {
// 	tests := []struct {
// 		name    string
// 		input   pb.PatientHistoryReq
// 		want    pb.PatientHistoryRes
// 		wantErr bool
// 	}{
// 		{
// 			name:    "success",
// 			input:   pb.PatientHistoryReq{PatientId: 1, PatientName: "Quvonchbek", IllinessName: "nmadir", Method: "nmadir", Result: "nmadir"},
// 			want:    pb.PatientHistoryRes{PatientId: 1, PatientName: "Quvonchbek", IllinessName: "nmadir", Method: "nmadir", Result: "nmadir"},
// 			wantErr: false,
// 		},
// 	}

// 	for _, tCase := range tests {
// 		t.Run(tCase.name, func(t *testing.T) {
// 			got, err := pgRepo.CreatePatientHistory(&tCase.input)
// 			if err != nil {
// 				t.Fatalf("%s: expected: %v, got: %v", tCase.name, tCase.wantErr, err)
// 			}

// 			tCase.want.CreatedAt = got.CreatedAt
// 			tCase.want.UpdatedAt = got.UpdatedAt
// 			if !reflect.DeepEqual(tCase.want, *got) {
// 				t.Fatalf("%s: expected: %v, got: %v", tCase.name, tCase.want, err)

// 			}

// 		})
// 	}
// }

// func TestPatientHistory_Get(t *testing.T) {
// 	tests := []struct {
// 		name    string
// 		input   pb.PatientHistoryName
// 		want    pb.PatientHistoryRes
// 		wantErr bool
// 	}{
// 		{
// 			name:    "success",
// 			input:   pb.PatientHistoryName{PatientName: "Quvonchbek"},
// 			want:    pb.PatientHistoryRes{PatientId: 1, PatientName: "Quvonchbek", IllinessName: "nmadir", Method: "nmadir", Result: "nmadir"},
// 			wantErr: false,
// 		},
// 	}

// 	for _, tCase := range tests {
// 		t.Run(tCase.name, func(t *testing.T) {
// 			got, err := pgRepo.GetPatientHistory(&tCase.input)
// 			if err != nil {
// 				t.Fatalf("%s: expected: %v, got: %v", tCase.name, tCase.wantErr, err)
// 			}

// 			tCase.want.CreatedAt = got.CreatedAt
// 			tCase.want.UpdatedAt = got.UpdatedAt
// 			if !reflect.DeepEqual(tCase.want, *got) {
// 				t.Fatalf("%s: expected: %v, got: %v", tCase.name, tCase.want, err)

// 			}
// 		})
// 	}
// }

// func TestPatientHistory_Update(t *testing.T) {
// 	tests := []struct {
// 		name    string
// 		input   pb.UpdatePatientHirtory
// 		want    pb.PatientHistoryRes
// 		wanrErr bool
// 	}{
// 		{
// 			name: "success",
// 			input: pb.UpdatePatientHirtory{
// 				PatientId:    1,
// 				IllinessName: "ppp",
// 				Method:       "ppp",
// 				Result:       "ppp"},
// 			want: pb.PatientHistoryRes{
// 				PatientId:    1,
// 				PatientName:  "Quvonchbek",
// 				IllinessName: "ppp",
// 				Method:       "ppp",
// 				Result:       "ppp"},
// 			wanrErr: false,
// 		},
// 	}

// 	for _, tCase := range tests {
// 		t.Run(tCase.name, func(t *testing.T) {
// 			got, err := pgRepo.UpdatePatientHistory(&tCase.input)
// 			if err != nil {
// 				t.Fatalf("%s: expected: %v, got: %v", tCase.name, tCase.wanrErr, err)
// 			}

// 			tCase.want.UpdatedAt = got.UpdatedAt
// 			if !reflect.DeepEqual(tCase.want, *got) {
// 				t.Fatalf("%s: expected: %v, got: %v", tCase.name, tCase.want, err)
// 			}
// 		})
// 	}
// }

// func TestPatientHistory_GetAll(t *testing.T) {
// 	tests := []struct {
// 		name    string
// 		input   pb.PatientHistoryListReq
// 		want    pb.PatientHistoryRes
// 		wantErr bool
// 	}{
// 		{
// 			name:  "success",
// 			input: pb.PatientHistoryListReq{Page: 1, Limit: 1},
// 			want: pb.PatientHistoryRes{
// 				PatientId:    1,
// 				PatientName:  "Quvonchbek",
// 				IllinessName: "ppp",
// 				Method:       "ppp",
// 				Result:       "ppp",
// 			},
// 			wantErr: false,
// 		},
// 	}

// 	for _, tCase := range tests {
// 		t.Run(tCase.name, func(t *testing.T) {
// 			got, err := pgRepo.GetAllPatientHistory(&tCase.input)
// 			if err != nil {
// 				t.Fatalf("%s: expected: %v, got: %v", tCase.name, tCase.wantErr, err)
// 			}
// 			patient := pb.PatientHistoryRes{}
// 			for _, val := range got.PatientsHistory {
// 				patient.CreatedAt = val.CreatedAt
// 				patient.UpdatedAt = val.UpdatedAt
// 				patient.IllinessName = val.IllinessName
// 				patient.Method = val.Method
// 				patient.Result = val.Result
// 				patient.PatientId = val.PatientId
// 				patient.PatientName = val.PatientName
// 			}
// 			tCase.want.CreatedAt = patient.CreatedAt
// 			tCase.want.UpdatedAt = patient.UpdatedAt

// 			if !reflect.DeepEqual(tCase.want, patient) {
// 				t.Fatalf("%s: expected: %v, got: %v", tCase.name, tCase.want, err)
// 			}
// 		})

// 	}
// }
