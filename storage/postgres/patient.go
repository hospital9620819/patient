package postgres

import (
	"log"
	"time"

	pb "hospital/patient/genproto/patient"
)


// Patient...
func (p *PatientRepo) CreatePatient(patient *pb.Patient) (*pb.PatientResponse, error) {
	var res pb.PatientResponse
	err := p.db.DB.QueryRow(`
	INSERT INTO patients(
		first_name, last_name, age, gender, email, diagnosis, illiness_name,
		phone_number, building_id, room_id, doctor_id, created_at) 
	VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)
	RETURNING 
		id, first_name, last_name, age, gender, email, diagnosis, illiness_name,
		phone_number, building_id, room_id ,doctor_id, created_at`,
		patient.FirstName, patient.LastName, patient.Age, patient.Gender,
		patient.Email, patient.Diagnosis, patient.IllinessName, patient.PhoneNumber,
		patient.BuildingId, patient.RoomId, patient.DoctorId, time.Now()).
		Scan(
			&res.PatientId, &res.FirstName, &res.LastName, &res.Age, &res.Gender, 
			&res.Email,	&res.Diagnosis, &res.IllinessName, &res.PhoneNumber, 
			&res.BuildingId, &res.RoomId, &res.DoctorId, &res.CreatedAt,
		)

	if err != nil {
		log.Println("failed to create patient")
		return &pb.PatientResponse{}, err
	}

	return &res, nil
}

func (p *PatientRepo) GetPatientByName(patient *pb.PatientName) (*pb.PatientResponse, error) {
	var response pb.PatientResponse
	err := p.db.QueryRow(`
	SELECT 
		id, first_name, last_name, age, gender, email, illiness_name,
		diagnosis, phone_number, building_id, room_id, doctor_id
	FROM 
		patients
	WHERE 
		first_name = $1 and deleted_at is NULL`, patient.PatientName).
		Scan(
			&response.PatientId, &response.FirstName, &response.LastName,
			&response.Age, &response.Gender, &response.Email, &response.IllinessName ,
			&response.Diagnosis, &response.PhoneNumber, &response.BuildingId,
			&response.RoomId, &response.DoctorId)

	if err != nil {
		log.Println("Failed to get patient")
		return &pb.PatientResponse{}, err
	}

	return &response, nil
}

func (p *PatientRepo) GetAllPatients(patient *pb.PatientListRequest) (*pb.Patients, error) {
	var response pb.Patients
	offset := (patient.Page - 1) * patient.Limit

	rows, err := p.db.Query(`
		SELECT 
			id,first_name, last_name, age, gender, email, illiness_name,
			diagnosis, phone_number, building_id, room_id, doctor_id
		FROM	
			patients
		WHERE 
			deleted_at is NULL
		LIMIT $1 OFFSET $2`, patient.Limit, offset)

	if err != nil {
		log.Println("Failed to get all patient")
		return &pb.Patients{}, nil
	}

	for rows.Next() {
		temp := pb.Patient{}

		err = rows.Scan(
			&temp.PatientId,
			&temp.FirstName,
			&temp.LastName,
			&temp.Age,
			&temp.Gender,
			&temp.Email,
			&temp.IllinessName,
			&temp.Diagnosis,
			&temp.PhoneNumber,
			&temp.BuildingId,
			&temp.RoomId,
			&temp.DoctorId,
		)

		if err != nil {
			log.Println("Failed to scan patient info")
			return &pb.Patients{}, err
		}

		response.Patients = append(response.Patients, &temp)
	}

	return &response, nil
}

func (p *PatientRepo) UpdatePatient(patient *pb.UpdatePatientInfo) (*pb.PatientResponse, error) {
	res := pb.PatientResponse{}
	err := p.db.QueryRow(`
		UPDATE 
			patients
		SET 
			diagnosis = $1, room_id = $2, doctor_id = $3, updated_at=NOW()
		WHERE 
			id = $4 AND deleted_at IS NULL
		RETURNING 
			id, first_name, last_name, age, gender, email, illiness_name,
			diagnosis, phone_number, building_id, room_id, doctor_id, updated_at`,
		patient.Diagnosis, patient.RoomId, patient.DoctorId, patient.Id).
		Scan(
			&res.PatientId, &res.FirstName, &res.LastName, &res.Age,
			&res.Gender, &res.Email, &res.IllinessName, &res.Diagnosis,
			&res.PhoneNumber, &res.BuildingId, &res.RoomId, &res.DoctorId, &res.UpdatedAt,
		)

	if err != nil {
		log.Println("Failed to update patient info: ", err)
		return &pb.PatientResponse{}, err
	}

	return &res, nil
}

func (p *PatientRepo) DeletePatient(patient *pb.PatientName) (*pb.PatientResponse, error) {
	res := pb.PatientResponse{}
	err := p.db.QueryRow(`
		UPDATE 
			patients
		SET 
			deleted_at = $1
		WHERE 
			first_name = $2
		RETURNING 
			id, first_name, last_name, age, gender, email, illiness_name,
			diagnosis, phone_number, building_id, room_id, doctor_id, deleted_at`, time.Now(), patient.PatientName).
		Scan(
			&res.PatientId, &res.FirstName, &res.LastName, &res.Age,
			&res.Gender, &res.Email, &res.IllinessName, &res.Diagnosis,
			&res.PhoneNumber, &res.BuildingId, &res.RoomId, &res.DoctorId, &res.DeletedAt,
		)

	if err != nil {
		log.Println("Failed to delete patient info")
		return &pb.PatientResponse{}, err
	}
	return &res, nil
}

// Patient history...
func (p *PatientRepo) CreatePatientHistory(req *pb.PatientHistoryReq) (*pb.PatientHistoryRes, error) {
	var res pb.PatientHistoryRes
	err := p.db.DB.QueryRow(`
	INSERT INTO patient_history(
		patient_id, first_name, last_name, age, gender, email, diagnosis, illiness_name,
		phone_number, room_id, doctor_id, medication_id, created_at) 
	VALUES 
		($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)
	RETURNING 
		patient_id, first_name, last_name, age, gender, email, diagnosis,
		illiness_name, phone_number, doctor_id, room_id, medication_id , created_at`,
		req.PatientId, req.FirstName, req.LastName, req.Age, req.Gender, req.Email, req.Diagnosis, 
		req.IllinessName, req.PhoneNumber, req.RoomId, req.DoctorId, req.MedicationId, time.Now()).
		Scan(
			&res.PatientId, &res.FirstName, &res.LastName,	&res.Age, &res.Gender, &res.Email, &res.Diagnosis,
			&res.IllinessName, &res.PhoneNumber, &res.DoctorId, &res.RoomId, &res.MedicationId ,&res.CreatedAt,
		)

	if err != nil {
		log.Println("failed to create patient info : ", err)
		return &pb.PatientHistoryRes{}, err
	}

	return &res, nil
}

func (p *PatientRepo) GetPatientHistory(req *pb.PatientName) (*pb.PatientHistoryRes, error) {
	var res pb.PatientHistoryRes
	err := p.db.DB.QueryRow(`
		SELECT 
			first_name, last_name, age, gender, email,illiness_name,
			diagnosis, phone_number, room_id, doctor_id, medication_id
		FROM 
			patient_history
		WHERE first_name = $1`,req.PatientName).Scan(
			&res.FirstName, &res.LastName, &res.Age, &res.Gender, &res.Email, &res.IllinessName,
			&res.Diagnosis, &res.PhoneNumber, &res.RoomId, &res.DoctorId, &res.MedicationId,
		)

	if err != nil {
		log.Println("fail to get patient history info:", err)
	}

	return &res, nil
}

func (p *PatientRepo) GetAllPatientHistory(req *pb.PatientListRequest) (*pb.PatientsHistoryList, error) {
	var res pb.PatientsHistoryList
	offset := (req.Page - 1) * req.Limit

	rows, err := p.db.Query(`
		SELECT 
			first_name, last_name, age, gender, email,illiness_name,
			diagnosis, phone_number, room_id, doctor_id, medication_id
		FROM	
			patient_history
		WHERE 
			deleted_at is NULL
		LIMIT $1 OFFSET $2`, req.Limit, offset)
	if err != nil {
		log.Println("fail to get all patient info:", err)
	}

	for rows.Next() {
		temp := pb.PatientHistoryRes{}

		err = rows.Scan(
			&temp.FirstName,
			&temp.LastName,
			&temp.Age,
			&temp.Gender,
			&temp.Email,
			&temp.IllinessName,
			&temp.Diagnosis,
			&temp.PhoneNumber,
			&temp.RoomId,
			&temp.DoctorId,
			&temp.MedicationId,
		)
		if err != nil {
			log.Println("fail to scan patient info:", err)
		}

		res.PatientsList = append(res.PatientsList, &temp)
	}

	return &res, nil
}

func (p *PatientRepo) UpdatePatientHistory(req *pb.PatientResponse) (*pb.PatientResponse, error) {
	var res pb.PatientResponse
	err := p.db.QueryRow(`
		UPDATE 
			patient_history
		SET 
			diagnosis = $1, room_id = $2, doctor_id = $3, updated_at=NOW()
		WHERE 
			patient_id = $4 AND deleted_at IS NULL
		RETURNING 
			patient_id, first_name, last_name, age, gender, email, illiness_name,
			diagnosis, phone_number, room_id, doctor_id, updated_at`,
			req.Diagnosis, req.RoomId, req.DoctorId, req.PatientId).
		Scan(
			&res.PatientId, &res.FirstName, &res.LastName, &res.Age,
			&res.Gender, &res.Email, &res.IllinessName, &res.Diagnosis,
			&res.PhoneNumber, &res.RoomId, &res.DoctorId, &res.UpdatedAt,
		)
	if err != nil {
		log.Println("fail to update patient history info:", err)
	}

	return &res, nil
}

func (p *PatientRepo) DeletePatientHistory(req *pb.PatientId) (*pb.PatientHistoryRes, error) {
	return nil, nil
}





// func (r *PatientRepo) SearchPatient(patient *pb.PatientName) (*pb.PatientResponse, error) {
// 	var res pb.PatientResponse

// 	err := r.db.QueryRow(`
// 		SELECT id,first_name,last_name,age,gender,email,diagnosis,room_id,branch_id,disease_id,employees_id,created_at,updated_at
// 		FROM patients
// 		WHERE first_name LIKE $1 AND deleted_at IS NULL
// 		`, patient.FirstName+"%").Scan(&res.Id, &res.FirstName, &res.LastName, &res.Age,
// 		&res.Gender, &res.Email, &res.Diagnosis, &res.RoomId,
// 		&res.BranchId, &res.DiseaseId, &res.EmployeesId, &res.CreatedAt, &res.UpdatedAt)

// 	if err != nil {
// 		log.Println("Failed to searching patient history info:. ", err)
// 		return &pb.PatientResponse{}, err
// 	}

// 	return &res, nil

// }

// func (r *PatientRepo) CheckField(check *pb.CheckFieldReq) (*pb.CheckFieldResp, error) {
// 	query := fmt.Sprintf("select 1 from patients where %s = $1", check.Field)
// 	var exists int

// 	err := r.db.QueryRow(query, check.Value).Scan(&exists)
// 	if err == sql.ErrNoRows {
// 		return &pb.CheckFieldResp{Exists: false}, nil
// 	}

// 	if err != nil {
// 		return &pb.CheckFieldResp{}, err
// 	}

// 	if exists == 0 {
// 		return &pb.CheckFieldResp{Exists: false}, nil
// 	}

// 	return &pb.CheckFieldResp{Exists: true}, nil
// }

// func (r *PatientRepo) GetByEmail(patient *pb.LoginRequest) (*pb.PatientRequest, error) {
// 	var response pb.PatientRequest

// 	err := r.db.QueryRow(`
// 		SELECT
// 			id,first_name,last_name,age,gender,email,diagnosis,room_id,branch_id,disease_id,
// 			employees_id, password ,accsess_token, refresh_token
// 		FROM
// 			patients
// 		WHERE email = $1`, patient.Email).
// 		Scan(&response.Id,
// 			&response.FirstName,
// 			&response.LastName,
// 			&response.Age,
// 			&response.Gender,
// 			&response.Email,
// 			&response.Diagnosis,
// 			&response.RoomId,
// 			&response.BranchId,
// 			&response.DiseaseId,
// 			&response.EmployeesId,
// 			&response.Password,
// 			&response.AccsessToken,
// 			&response.RefreshToken)

// 	if err != nil {
// 		log.Println("Failed to get login")
// 		return &pb.PatientRequest{}, err
// 	}

// 	return &response, nil
// }

// func (r *PatientRepo) UpdatePatientTokens(patient *pb.UpdatePatientTokensReq) (*pb.UpdatePatientTokensReq, error) {
// 	res := pb.UpdatePatientTokensReq{}

// 	err := r.db.QueryRow(`
// 		UPDATE patients
// 		SET accsess_token = $1, refresh_token = $2
// 		WHERE id=$3
// 		RETURNING id,accsess_token,refresh_token`, patient.AccsessToken, patient.RefreshToken, patient.Id).Scan(
// 		&res.Id, &res.AccsessToken, &res.RefreshToken,
// 	)

// 	if err != nil {
// 		log.Println("Failed to update patient info: ", err)
// 		return &pb.UpdatePatientTokensReq{}, nil
// 	}

// 	return &res, nil
// }

// func (r *PatientRepo) GetPatientById(patient *pb.PatientIdReq) (*pb.PatientRequest, error) {
// 	var response pb.PatientRequest

// 	err := r.db.QueryRow(`
// 		SELECT
// 			id,first_name,last_name,age,gender,email,diagnosis,room_id,branch_id,disease_id,
// 			employees_id, password ,accsess_token, refresh_token
// 		FROM
// 			patients
// 		WHERE id = $1`, patient.Id).
// 		Scan(&response.Id,
// 			&response.FirstName,
// 			&response.LastName,
// 			&response.Age,
// 			&response.Gender,
// 			&response.Email,
// 			&response.Diagnosis,
// 			&response.RoomId,
// 			&response.BranchId,
// 			&response.DiseaseId,
// 			&response.EmployeesId,
// 			&response.Password,
// 			&response.AccsessToken,
// 			&response.RefreshToken)

// 	if err != nil {
// 		log.Println("Failed to get by id")
// 		return &pb.PatientRequest{}, err
// 	}

// 	return &response, nil
// }
