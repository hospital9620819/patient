package storage

import (
	"hospital/patient/storage/postgres"
	"hospital/patient/storage/repo"

	"github.com/jmoiron/sqlx"
)

type IStorage interface {
	Patient()  repo.PatientStorageI
}

type storagePg struct {
	db          *sqlx.DB
	patientRepo repo.PatientStorageI
}

func NewStoragePg(db *sqlx.DB) *storagePg {
	return &storagePg{
		db:          db,
		patientRepo: postgres.NewPatientRepo(db),
	}
}

func (s storagePg) Patient() repo.PatientStorageI {
	return s.patientRepo
}