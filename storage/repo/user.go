package repo

import pb "hospital/patient/genproto/patient"

type PatientStorageI interface {
	// Patient info
	CreatePatient(*pb.Patient) (*pb.PatientResponse, error)
	GetPatientByName(*pb.PatientName) (*pb.PatientResponse, error)
	GetAllPatients(*pb.PatientListRequest) (*pb.Patients, error)
	UpdatePatient(*pb.UpdatePatientInfo) (*pb.PatientResponse, error)
	DeletePatient(*pb.PatientName) (*pb.PatientResponse, error)

	// Patient history info
	CreatePatientHistory(*pb.PatientHistoryReq) (*pb.PatientHistoryRes, error)
	GetPatientHistory(*pb.PatientName) (*pb.PatientHistoryRes, error)
	GetAllPatientHistory(*pb.PatientListRequest) (*pb.PatientsHistoryList, error)
	UpdatePatientHistory(*pb.PatientResponse) (*pb.PatientResponse, error)
	DeletePatientHistory(*pb.PatientName) (*pb.PatientResponse, error)


	// SearchPatientHistory(*pb.PatientHistoryName) (*pb.PatientHistoryRes, error)
	// SearchPatient(*pb.PatientName) (*pb.PatientResponse, error)
	// CheckField(*pb.CheckFieldReq) (*pb.CheckFieldResp, error)
	// GetByEmail(*pb.LoginRequest) (*pb.PatientRequest, error)
	// GetPatientById(*pb.PatientIdReq) (*pb.PatientRequest, error)
	// UpdatePatientTokens(*pb.UpdatePatientTokensReq) (*pb.UpdatePatientTokensReq, error)
}
