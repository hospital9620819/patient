package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	PostgresHost       string
	PostgresPort       int64
	PostgresUser       string
	PostgresDatabase   string
	PostgresPassword   string
	Environment        string
	LogLevel           string

	PatientServiceHost string
	PatientServicePort string

	IllinesServiceHost string
	IllinesServicePort string
}

func Load() Config {
	c := Config{}

	c.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToInt64(getOrReturnDefault("POSTGRES_PORT", 5432))
	c.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "hospital"))
	c.PostgresDatabase = cast.ToString(getOrReturnDefault("POSTGRES_DATABASE", "patient"))
	c.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "h"))
	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "developer"))
	c.LogLevel = cast.ToString(getOrReturnDefault("LOG_LEVEL", "debug"))

	c.PatientServiceHost = cast.ToString(getOrReturnDefault("PATIENT_SERVICE_HOST", "localhost"))
	c.PatientServicePort = cast.ToString(getOrReturnDefault("PATIENT_SERVICE_PORT", ":5000"))

	c.IllinesServiceHost = cast.ToString(getOrReturnDefault("ILLINES_SERVICE_HOST", "localhost"))
	c.IllinesServicePort = cast.ToString(getOrReturnDefault("ILLINES_SERVICE_PORT", "6000"))

	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
